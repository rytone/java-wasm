#!/bin/bash

set -e
mkdir -p build

echo ">>> Building 'add' WASM library..."
cd add
cargo build --target wasm32-unknown-unknown --release
cp target/wasm32-unknown-unknown/release/add.wasm ../build
cd ..

echo ">>> Building 'runtime' Rust library..."
cd runtime
cargo build --release
cp target/release/libruntime.so ../build
cd ..

echo ">>> Building Java main class..."
cd java
javac Main.java
mv Main.class ../build
cd ..

echo "Done! Enter the build folder and run with 'LD_LIBRARY_PATH=. java Main'"
