public class Main {
    private static native int add(int a, int b);

    static {
        System.loadLibrary("runtime");
    }

    public static void main(String[] args) {
        int res = Main.add(1, 2);
        System.out.println("add(1, 2) => " + res);
    }
}
