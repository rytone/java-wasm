use std::fs::File;
use std::io::Read;

use jni::{
    JNIEnv,
    objects::JClass,
    sys::jint,
};

use snafu::{Snafu, ResultExt};
use wasmer_runtime::Func;

#[derive(Debug, Snafu)]
enum Error {
    #[snafu(display("could not read wasm: {}", source))]
    LoadError { source: std::io::Error },
    #[snafu(display("could not create wasm instance: {}", source))]
    InstanceError { source: wasmer_runtime::error::Error },
    #[snafu(display("failed to find add(i32, i32) -> i32: {}", source))]
    FunctionError { source: wasmer_runtime::error::ResolveError },
    #[snafu(display("failed to call add({}, {}): {}", params.0, params.1, source))]
    CallError { params: (i32, i32), source: wasmer_runtime::error::RuntimeError }
}

// TODO don't create a new wasm instance on every function call
// it can probably be done with lazy_static lol
fn wasm_add(a: i32, b: i32) -> Result<i32, Error> {
    let mut file = File::open("./add.wasm").context(LoadError)?;
    
    let mut bytes = Vec::new();
    file.read_to_end(&mut bytes).context(LoadError)?;

    let import_object = wasmer_runtime::imports! {};
    let instance = wasmer_runtime::instantiate(&bytes, &import_object)
        .context(InstanceError)?;

    let add_fn: Func<(i32, i32), i32> = instance.func("add")
        .context(FunctionError)?;

    let res = add_fn.call(a, b).context(CallError { params: (a, b) })?;

    Ok(res)
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "system" fn Java_Main_add(
    env: JNIEnv,
    _class: JClass,
    a: jint,
    b: jint,
) -> jint {
    match wasm_add(a, b) {
        Ok(res) => res,
        Err(e) => {
            env.throw(format!("{}", e)).expect("could not throw error");
            0
        }
    }
}
